#!/bin/bash

#URL_DOWNLOAD_ARTIFACT="$CI_API_V4_URL/projects/$CI_PROJECT_ID/jobs/artifacts/$CI_COMMIT_BRANCH/download?job=$NAME_LAST_JOB&private_token=$TOKEN_PROJECT" - Ссылка на все джобы артифакты с гитлабы (NAME_LAST_JOB-задаем руками в джобе)
#URL_DOWNLOAD_ARTIFACT_JAR="$CI_API_V4_URL/projects/$CI_PROJECT_ID/jobs/artifacts/$CI_COMMIT_BRANCH/raw/sausage-store-${VERSION}/sausage-store-${VERSION}.jar/?job=$NAME_LAST_JOB&private_token=$TOKEN_PROJECT" - Ссылка на файл артифакты с гитлабы (NAME_LAST_JOB-задаем руками в джобе)

#URL_DOWNLOAD_ARTIFACT_NEXUS_FRONTEND="$NEXUS_REPO_URL_FRONTEND/${VERSION}/sausage-store-${VERSION}.tar.gz" # Ссылка на артифакты с NEXUS
#URL_DOWNLOAD_ARTIFACT_NEXUS_BACKEND="$NEXUS_REPO_URL/com/yandex/practicum/devops/sausage-store/${VERSION}/sausage-store-${VERSION}.jar" # Ссылка на артифакты с NEXUS

URL_SONARCUBE_FRONTEND="https://sonarqube.praktikum-services.ru/dashboard?id=15_VLADIMIRTELUYK_FRONTEND" 
URL_SONARCUBE_BECKEND="https://sonarqube.praktikum-services.ru/dashboard?id=15_VLADIMIR.TELUYK_BACKEND"

URL_DEPLOY_FRONTEND="http://std-015-12.praktikum-services.tech:8080/"
#URL_DEPLOY_BECKEND="http://std-015-12.praktikum-services.tech:8080/"

URL="https://api.telegram.org/$TELEGRAM_BOT_TOKEN/sendMessage"

notify(){
    if [ "$VERSIONJOB" = "frontend" ]; then
        curl -X POST -H 'Content-type:application/json' --data '{"chat_id":"'"$TG_NUM_GR"'", "parse_mode":"Markdown", "text":"
        Вышла новая версия '"$VERSIONJOB"' сосисочной — '"$VERSION"'

        Sonarqube = [Открыть - 🕸]('"${URL_SONARCUBE_FRONTEND}"')

        URL deploy = [Frontend]('"${URL_DEPLOY_FRONTEND}"')

        Аuthor = '"$GITLAB_USER_NAME"'  "}' $URL

    elif [ "$VERSIONJOB" = "backend" ]; then
        curl -X POST -H 'Content-type:application/json' --data '{"chat_id":"'"$TG_NUM_GR"'", "parse_mode":"Markdown", "text":"
        Вышла новая версия '"$VERSIONJOB"' сосисочной — '"$VERSION"'
        
        Sonarqube = [Открыть - 🕸]('"${URL_SONARCUBE_BECKEND}"')

        Аuthor = '"$GITLAB_USER_NAME"'  "}' $URL
        
    else
        echo "Что-то пошло не так!"
    fi
}
notify
