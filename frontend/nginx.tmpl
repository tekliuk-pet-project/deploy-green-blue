{{ range $host, $containers := groupBy $ "Env.VIRTUAL_HOST" }}
upstream {{ $host }} {

{{ range $index, $value := $containers }}
    {{ with $address := index $value.Addresses 0 }}
    server {{ $value.Hostname }}:{{ $address.Port }};
    {{ end }}
{{ end }}

}
proxy_cache_path /var/cache/nginx levels=1:2 keys_zone=my_cache:10m inactive=60m;

server {
  listen 80;
  listen 443 ssl;
  server_name std-015-12.praktikum-services.tech;
  ssl_certificate /etc/letsencrypt/archive/std-015-12.praktikum-services.tech/fullchain.pem;
  ssl_certificate_key /etc/letsencrypt/archive/std-015-12.praktikum-services.tech/privkey.pem;

  ssl_session_cache   shared:SSL:1m;
  ssl_session_timeout 10m;
  keepalive_timeout 70;
  ssl_protocols TLSv1 TLSv1.1 TLSv1.2;
  ssl_prefer_server_ciphers on; 
  
  proxy_cache my_cache;
  proxy_cache_valid 200 60m;
  proxy_cache_valid 404 1m;
  proxy_cache_key "$scheme$request_method$host$request_uri";

  location / {
    root   /usr/share/nginx/html;
    index  index.html;
    try_files $uri $uri/ /index.html;
  }

  location /api {
      proxy_pass http://{{ $host }};
  }
}
{{ end }}
